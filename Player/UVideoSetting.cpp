////////////////////////////////////////////////////////////////
//  Player
//  ����� � ����������� �����
////////////////////////////////////////////////////////////////
#include <vcl.h>
#pragma hdrstop
#include "UVideoSetting.h"

//==============================================================
#pragma package(smart_init)
#pragma link "cspin"
#pragma resource "*.dfm"

//==============================================================
TFormVideoSetting *FormVideoSetting;

//==============================================================
//  ����������� � ����������
//==============================================================
__fastcall TFormVideoSetting::TFormVideoSetting(TComponent *Owner) :
	TForm(Owner)
{
}

//==============================================================
//  ������������� ����������� �����
//==============================================================
void TFormVideoSetting::Init(int VideoWidth, int VideoHeight,
	int VideoTransparent)
{
	CSpinEditVideoWidth->OnChange = NULL;
	CSpinEditVideoHeight->OnChange = NULL;
	TrackBarTransparent->OnChange = NULL;

	CSpinEditVideoWidth->Value = VideoWidth;
	CSpinEditVideoHeight->Value = VideoHeight;
	TrackBarTransparent->Position = VideoTransparent;

	CSpinEditVideoWidth->OnChange = CSpinEditVideoWidthChange;
	CSpinEditVideoHeight->OnChange = CSpinEditVideoHeightChange;
	TrackBarTransparent->OnChange = TrackBarTransparentChange;
}

//==============================================================
//  ������� ���������� ��� ������� �� ������ "�������"
//==============================================================
void __fastcall TFormVideoSetting::BtnCloseClick(TObject *Sender)
{
	Hide();
}

//==============================================================
//  ��������� ������� ��� ��������� �������� �����
//==============================================================
void TFormVideoSetting::RaiseVideoSettingEvent()
{
	if (OnVideoSettingChanged != NULL)
	{
		int VideoWidth = CSpinEditVideoWidth->Value;
		int VideoHeight = CSpinEditVideoHeight->Value;
		int VideoTransparent = TrackBarTransparent->Position;

		OnVideoSettingChanged(this, VideoWidth, VideoHeight, VideoTransparent);
	}
}

//==============================================================
//  ������� ���������� ��� ��������� ������ �����
//==============================================================
void __fastcall TFormVideoSetting::CSpinEditVideoWidthChange(TObject *Sender)
{
	RaiseVideoSettingEvent();
}

//==============================================================
//  ������� ���������� ��� ��������� ������ �����
//==============================================================
void __fastcall TFormVideoSetting::CSpinEditVideoHeightChange(TObject *Sender)
{
	RaiseVideoSettingEvent();
}

//==============================================================
//  ������� ���������� ��� ��������� ������������ �����
//==============================================================
void __fastcall TFormVideoSetting::TrackBarTransparentChange(TObject *Sender)
{
	RaiseVideoSettingEvent();
}

