////////////////////////////////////////////////////////////////
//  Player
//  ����� � �����
////////////////////////////////////////////////////////////////
#include <vcl.h>
#pragma hdrstop
#include "UVideo.h"

//==============================================================
#pragma package(smart_init)
#pragma link "WMPLib_OCX"
#pragma resource "*.dfm"

//==============================================================
TFormVideo *FormVideo;

//==============================================================
//  ����������� � ����������
//==============================================================
__fastcall TFormVideo::TFormVideo(TComponent *Owner) :
	TForm(Owner),
	IsGrabMouse(false)
{
}

//==============================================================
//  ������� ������������ �����
//==============================================================
void TFormVideo::PlayFile(String Path, String Title)
{
	WMPlayer->controls->stop();
	WMPlayer->URL = Path;
	WMPlayer->controls->play();

	EditTitle->Width = Canvas->TextExtent(Title).Width + 1;
	EditTitle->Height = Canvas->TextExtent(Title).Height + 1;
	EditTitle->Text = Title;
}

//==============================================================
//  ������� ���������� ��� ������� ���� �� ������� �����
//==============================================================
void __fastcall TFormVideo::WMPlayerMouseDown(TObject *Sender,
	short nButton, short nShiftState, long fX, long fY)
{
	if (nButton == 1)
	{
		IsGrabMouse = true;
		GrabMousePoint = TPoint(fX, fY);
	}
}

//==============================================================
//  ������� ���������� ��� ����������� ���� ��� �������� �����
//==============================================================
void __fastcall TFormVideo::WMPlayerMouseMove(TObject *Sender,
	short nButton, short nShiftState, long fX, long fY)
{
	if (IsGrabMouse)
	{
		int ShiftX = fX - GrabMousePoint.X;
		int ShiftY = fY - GrabMousePoint.Y;

		Left += ShiftX;
		Top += ShiftY;
	}
}

//==============================================================
//  ������� ���������� ��� ���������� ���� �� ������� �����
//==============================================================
void __fastcall TFormVideo::WMPlayerMouseUp(TObject *Sender, short nButton,
	short nShiftState, long fX, long fY)
{
	IsGrabMouse = false;
}

