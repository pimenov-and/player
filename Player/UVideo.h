////////////////////////////////////////////////////////////////
//  Player
//  ����� � �����
////////////////////////////////////////////////////////////////
#ifndef UVideoH
#define UVideoH

//==============================================================
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "WMPLib_OCX.h"
#include <Vcl.OleCtrls.hpp>

//==============================================================
//  ����� � �����
//==============================================================
class TFormVideo : public TForm
{
__published:
	TWindowsMediaPlayer *WMPlayer;
	TEdit *EditTitle;
	void __fastcall WMPlayerMouseDown(TObject *Sender, short nButton, short nShiftState,
          long fX, long fY);
	void __fastcall WMPlayerMouseMove(TObject *Sender, short nButton, short nShiftState,
          long fX, long fY);
	void __fastcall WMPlayerMouseUp(TObject *Sender, short nButton, short nShiftState,
          long fX, long fY);
private:
	//  ������� ������� ����� �����
	bool IsGrabMouse;
	//  ������� ���� ��� �������
	TPoint GrabMousePoint;
public:
	//  ����������� � ����������
	__fastcall TFormVideo(TComponent *Owner);

	//  ������� ������������ �����
	void PlayFile(String Path, String Title);
};

//==============================================================
extern PACKAGE TFormVideo *FormVideo;

//==============================================================
#endif
