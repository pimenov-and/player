////////////////////////////////////////////////////////////////
//  Player
//  ������� �����
////////////////////////////////////////////////////////////////
#include <vcl.h>
#pragma hdrstop
#include "UMain.h"
#include "UVideo.h"
#include "UVideoSetting.h"

//==============================================================
#pragma package(smart_init)
#pragma resource "*.dfm"

//==============================================================
TFormMain *FormMain;

//==============================================================
//  ����������� � ����������
//==============================================================
__fastcall TFormMain::TFormMain(TComponent *Owner) :
	TForm(Owner)
{
}

//==============================================================
//  ������� ���������� ��� ������� �� ������ "������� �����..."
//==============================================================
void __fastcall TFormMain::BtnOpenFilesClick(TObject *Sender)
{
	if (OpenDlg->Execute())
	{
		Playlist.clear();
		ListBoxFiles->Items->Clear();

		const int FileCount = OpenDlg->Files->Count;
		for (int i = 0; i < FileCount; ++i)
		{
			String Path = OpenDlg->Files->Strings[i];
			Playlist.push_back(Path);

			String FileName = ExtractFileName(Path);
			String ItemName = IntToStr(i + 1) + ". " + FileName;
			ListBoxFiles->Items->Add(ItemName);
		}
	}
}

//==============================================================
//  ������� ���������� ��� ������� �� ������ "��������� �����..."
//==============================================================
void __fastcall TFormMain::BtnVideoSettingClick(TObject *Sender)
{
	FormVideoSetting->OnVideoSettingChanged = VideoSettingChanged;

	int VideoWidth = FormVideo->Width;
	int VideoHeight = FormVideo->Height;
	int VideoTransparent = FormVideo->AlphaBlendValue;
	FormVideoSetting->Init(VideoWidth, VideoHeight, VideoTransparent);

	FormVideoSetting->Show();
}

//==============================================================
//  ������� ���������� ��� ��������� �������� �����
//==============================================================
void __fastcall VideoSettingChanged(TObject *Sender, int VideoWidth,
	int VideoHeight, int VideoTransparent)
{
	FormVideo->Width = VideoWidth;
	FormVideo->Height = VideoHeight;
	FormVideo->AlphaBlendValue = VideoTransparent;
}

//==============================================================
//  ������� ���������� ��� ������� ������ �� ������ ������
//==============================================================
void __fastcall TFormMain::ListBoxFilesDblClick(TObject *Sender)
{
	const int SelIndex = ListBoxFiles->ItemIndex;
	if (SelIndex == -1)
		return;

	FormVideo->Show();

	String Path = Playlist[SelIndex];
	String Title = ListBoxFiles->Items->Strings[SelIndex];
	FormVideo->PlayFile(Path, Title);
}

//==============================================================
//  ������� ���������� ��� ��������� �������� �����
//==============================================================
void __fastcall TFormMain::VideoSettingChanged(TObject *Sender,
	int VideoWidth, int VideoHeight, int VideoTransparent)
{
	FormVideo->Width = VideoWidth;
	FormVideo->Height = VideoHeight;
	FormVideo->AlphaBlendValue = VideoTransparent;
}

