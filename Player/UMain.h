////////////////////////////////////////////////////////////////
//  Player
//  ������� �����
////////////////////////////////////////////////////////////////
#ifndef UMainH
#define UMainH

//==============================================================
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>
#include <vector>

//==============================================================
//  ������� �����
//==============================================================
class TFormMain : public TForm
{
__published:	// IDE-managed Components
	TButton *BtnOpenFiles;
	TButton *BtnVideoSetting;
	TListBox *ListBoxFiles;
	TOpenDialog *OpenDlg;
	void __fastcall BtnOpenFilesClick(TObject *Sender);
	void __fastcall BtnVideoSettingClick(TObject *Sender);
	void __fastcall ListBoxFilesDblClick(TObject *Sender);
private:
	//  ������� ���������� ��� ��������� �������� �����
	void __fastcall VideoSettingChanged(TObject *Sender, int VideoWidth,
		int VideoHeight, int VideoTransparent);

	//  ��������
	std::vector<String> Playlist;
public:
	//  ����������� � ����������
	__fastcall TFormMain(TComponent *Owner);
};

//==============================================================
extern PACKAGE TFormMain *FormMain;

//==============================================================
#endif // UMainH
