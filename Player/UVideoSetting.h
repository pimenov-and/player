////////////////////////////////////////////////////////////////
//  Player
//  ����� � ����������� �����
////////////////////////////////////////////////////////////////
#ifndef UVideoSettingH
#define UVideoSettingH

//==============================================================
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cspin.h"
#include <Vcl.ComCtrls.hpp>

//==============================================================
//  ��� �������, ������������ ��� ��������� �������� �����
typedef void __fastcall(__closure *TVideoSettingChangedEvent)(TObject *Sender,
	int VideoWidth, int VideoHeight, int VideoTransparent);

//==============================================================
//  ����� � ����������� �����
//==============================================================
class TFormVideoSetting : public TForm
{
__published:
	TLabel *LabelVideoWidth;
	TCSpinEdit *CSpinEditVideoWidth;
	TCSpinEdit *CSpinEditVideoHeight;
	TLabel *LabelVideoHeight;
	TLabel *LabelTransparent;
	TTrackBar *TrackBarTransparent;
	TButton *BtnClose;
	void __fastcall BtnCloseClick(TObject *Sender);
	void __fastcall CSpinEditVideoWidthChange(TObject *Sender);
	void __fastcall CSpinEditVideoHeightChange(TObject *Sender);
	void __fastcall TrackBarTransparentChange(TObject *Sender);
private:
	//  ��������� ������� ��� ��������� �������� �����
	void RaiseVideoSettingEvent();
public:
	//  ����������� � ����������
	__fastcall TFormVideoSetting(TComponent *Owner);

	//  ������������� ����������� �����
	void Init(int VideoWidth, int VideoHeight, int VideoTransparent);

	//  ������� ��������� ��� ��������� �������� �����
	TVideoSettingChangedEvent OnVideoSettingChanged;
};

//==============================================================
extern PACKAGE TFormVideoSetting *FormVideoSetting;

//==============================================================
#endif // UVideoSettingH
